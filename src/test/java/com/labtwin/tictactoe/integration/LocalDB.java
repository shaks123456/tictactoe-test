package com.labtwin.tictactoe.integration;

import com.amazonaws.services.dynamodbv2.local.main.ServerRunner;
import com.amazonaws.services.dynamodbv2.local.server.DynamoDBProxyServer;

import java.io.IOException;
import java.net.ServerSocket;

public class LocalDB {

    private static DynamoDBProxyServer server;

    protected static void before() throws Exception {
        System.setProperty("sqlite4java.library.path", "native-libs");
        String port = getAvailablePort();
        server = ServerRunner.createServerFromCommandLineArgs(new String[]{"-inMemory", "-port", port});
        server.start();
    }

    protected static void after() {
        stopUnchecked(server);
    }

    private static void stopUnchecked(DynamoDBProxyServer dynamoDbServer) {
        try {
            dynamoDbServer.stop();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static String getAvailablePort() throws IOException {
        ServerSocket serverSocket = new ServerSocket(0);
        return String.valueOf(serverSocket.getLocalPort());
    }

}
