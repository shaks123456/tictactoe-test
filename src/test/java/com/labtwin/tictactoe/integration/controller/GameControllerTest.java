package com.labtwin.tictactoe.integration.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.labtwin.tictactoe.integration.BaseTest;
import com.labtwin.tictactoe.model.PlayerInfo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class GameControllerTest extends BaseTest {

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected ObjectMapper mapper;

    /**
     * Scenario: Create Games
     * Expected Behaviour: Should create Game successfully.
     */
    @Test
    void testCreateGames() throws Exception {
        PlayerInfo info = new PlayerInfo();
        info.setPlayerX("player a");
        info.setPlayerX("player b");
        MvcResult result = mockMvc.perform(post("/labtwin/api/v1/games")
                .content(mapper.writeValueAsString(info))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        String response = result.getResponse().getContentAsString();
        Assertions.assertNotNull(response);
    }
}

