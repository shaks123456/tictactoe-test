package com.labtwin.tictactoe.controller;

import com.labtwin.tictactoe.dto.Game;
import com.labtwin.tictactoe.dto.Move;
import com.labtwin.tictactoe.model.PlayerInfo;
import com.labtwin.tictactoe.service.GameService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(Endpoints.BASE_URI)
public class GameController {

    private final GameService gameService;

    @Autowired
    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @GetMapping(value = Endpoints.HEALTH_URI, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    @ApiOperation(value = "Health check", response = Map.class,
            notes = "Health check API"
    )
    public Map<String, String> getHealth() {
        return Collections.singletonMap("status", "ok");
    }

    @PostMapping(value = Endpoints.GAME_URI, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    @ApiOperation(value = "Create a tictactoe game with players.", response = Map.class,
            notes = "Creates a game with two players"
    )
    public Map<String, String> createGame(@RequestBody PlayerInfo playerInfo) {
        return this.gameService.createGame(playerInfo);
    }

    @PutMapping(value = Endpoints.GAME_MOVE_URI, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    @ApiOperation(value = "Makes a move in the Game", response = Game.class,
            notes = "Player makes a move by specifying the position"
    )
    public Game makeAMove(@PathVariable String gameId, @RequestBody Move move) {
        return this.gameService.makeAMove(gameId, move);
    }

    @GetMapping(value = Endpoints.GAME_LIST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    @ApiOperation(value = "List games", response = List.class,
            notes = "List all the games"
    )
    public List<Game> getAllGames(@RequestParam int limit) throws IOException {
        return this.gameService.getAllGames(limit);
    }

}
