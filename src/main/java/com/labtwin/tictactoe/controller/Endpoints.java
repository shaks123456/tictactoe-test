package com.labtwin.tictactoe.controller;

final class Endpoints {
    static final String BASE_URI = "/labtwin/api";
    static final String GAME_URI = "/v1/games";
    static final String GAME_LIST = "/v1/gamesList";
    static final String GAME_MOVE_URI = "/v1/games/{gameId}/moves";
    static final String HEALTH_URI = "/v1/health";

    private Endpoints() {
    }
}
