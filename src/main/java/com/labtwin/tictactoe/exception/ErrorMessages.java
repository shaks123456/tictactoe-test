package com.labtwin.tictactoe.exception;


public final class ErrorMessages {

    public static final String GAME_ID_NOT_FOUND = "The game id is not found. Please check the game Id entered.";
    public static final String WRONG_MOVE_POSITION = "The move indexes are wrong, please provide a correct move index.";
    public static final String MOVE_BY_SAME_PLAYER = "The move was made by the same player as the previous move. Please change the player";
    public static final String MOVE_INDEX_TAKEN = "The move indexes are already taken, please change the index and move again";

    private ErrorMessages() {
    }
}

