package com.labtwin.tictactoe.model;

public class PlayerInfo {
    private String playerX;
    private String playerY;

    public String getPlayerX() {
        return playerX;
    }

    public void setPlayerX(String playerX) {
        this.playerX = playerX;
    }

    public String getPlayerY() {
        return playerY;
    }

    public void setPlayerY(String playerY) {
        this.playerY = playerY;
    }
}
