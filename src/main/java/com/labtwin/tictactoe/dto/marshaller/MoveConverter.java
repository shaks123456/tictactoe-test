package com.labtwin.tictactoe.dto.marshaller;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.labtwin.tictactoe.dto.Move;
import com.labtwin.tictactoe.exception.GameServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class MoveConverter implements DynamoDBTypeConverter<String, List<Move>> {

    private final ObjectMapper objectMapper = new ObjectMapper();
    Logger logger = LoggerFactory.getLogger(MoveConverter.class);

    @Override
    public String convert(List<Move> move) {
        String moveString = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            moveString = mapper.writeValueAsString(move);
        } catch (Exception e) {
            logger.error("Issue with move type conversion from object to string");
            throw new GameServiceException("Issue with converting the move object");
        }
        return moveString;
    }

    @Override
    public List<Move> unconvert(String moveString) {
        List<Move> move = null;
        try {
            move = objectMapper.readValue(moveString, new TypeReference<List<Move>>() {
            });
        } catch (Exception e) {
            logger.error("Issue with move type conversion from string to object");
            throw new GameServiceException("Issue with converting the move object");
        }
        return move;
    }
}