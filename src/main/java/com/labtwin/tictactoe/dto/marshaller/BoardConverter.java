package com.labtwin.tictactoe.dto.marshaller;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.labtwin.tictactoe.exception.GameServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BoardConverter implements DynamoDBTypeConverter<String, String[][]> {

    private final ObjectMapper objectMapper = new ObjectMapper();

    Logger logger = LoggerFactory.getLogger(BoardConverter.class);

    @Override
    public String convert(String[][] board) {
        String boardString = null;
        try {
            boardString = objectMapper.writeValueAsString(board);
        } catch (Exception e) {
            logger.error("Issue with board type conversion from object to string");
            throw new GameServiceException("Issue with converting the board object");
        }
        return boardString;
    }

    @Override
    public String[][] unconvert(String s) {
        String[][] board = null;
        try {
            board = objectMapper.readValue(s, String[][].class);
        } catch (Exception e) {
            logger.error("Issue with board type conversion from string to object");
            throw new GameServiceException("Issue with converting the board object");
        }
        return board;
    }
}