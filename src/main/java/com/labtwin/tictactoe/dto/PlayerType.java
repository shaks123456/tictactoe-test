package com.labtwin.tictactoe.dto;

public enum PlayerType {
    PLAYER_X("X"),
    PLAYER_Y("Y"),
    BLANK ("_");

    private final String playerValue;

    PlayerType(String playerValue) {
        this.playerValue = playerValue;
    }

    public String playerValue(){
        return this.playerValue;
    }
}
