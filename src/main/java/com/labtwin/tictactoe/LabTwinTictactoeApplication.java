package com.labtwin.tictactoe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LabTwinTictactoeApplication {

    public static void main(String[] args) {
        SpringApplication.run(LabTwinTictactoeApplication.class, args);
    }

}
