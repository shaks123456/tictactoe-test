package com.labtwin.tictactoe.service;

import com.labtwin.tictactoe.dao.GameDao;
import com.labtwin.tictactoe.dto.Game;
import com.labtwin.tictactoe.dto.Move;
import com.labtwin.tictactoe.dto.PlayerType;
import com.labtwin.tictactoe.exception.ErrorMessages;
import com.labtwin.tictactoe.exception.GameServiceException;
import com.labtwin.tictactoe.exception.NotFoundException;
import com.labtwin.tictactoe.model.PlayerInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class GameService implements IGameSerice {

    private final GameDao gameDao;

    @Autowired
    public GameService(GameDao gameDao) {
        this.gameDao = gameDao;
    }

    @Override
    public Map<String, String> createGame(PlayerInfo playerInfo) {
        Game game = new Game();
        game.setPlayerX(playerInfo.getPlayerX());
        game.setPlayerY(playerInfo.getPlayerY());

        Game gameResponse = gameDao.upsertGame(game);
        return Collections.singletonMap("gameId", gameResponse.getGameId());
    }

    @Override
    public Game makeAMove(String gameId, Move move) {
        Game game = Optional.ofNullable(this.gameDao.getGame(gameId))
                .orElseThrow(() -> new NotFoundException(ErrorMessages.GAME_ID_NOT_FOUND));
        if (game.getWinnerStatus() != null) {
            return game;
        }
        GameService.validateMoves(move, game);
        if (game.getLastMoveBy() == PlayerType.PLAYER_X) {
            game.setLastMoveBy(PlayerType.PLAYER_Y);
            move.setPlayerType(PlayerType.PLAYER_Y);
        } else {
            game.setLastMoveBy(PlayerType.PLAYER_X);
            move.setPlayerType(PlayerType.PLAYER_X);
        }
        game.addMoves(move);
        game.setAMove(move);
        evaluateWinner(move, game);
        return this.gameDao.upsertGame(game);
    }

    @Override
    public List<Game> getAllGames(int limit) {
        return gameDao.scan(limit);
    }

    private static void validateMoves(Move move, Game game) {
        if (move.getRow() > 2 || move.getRow() < 0 || move.getColumn() > 2 || move.getColumn() < 0) {
            throw new GameServiceException(ErrorMessages.WRONG_MOVE_POSITION);
        }
        if (move.getPlayerType() != null && game.getLastMoveBy() == move.getPlayerType()) {
            throw new GameServiceException(ErrorMessages.MOVE_BY_SAME_PLAYER);
        }
        if (!(game.getBoard()[move.getRow()][move.getColumn()]).equals(PlayerType.BLANK.playerValue())) {
            throw new GameServiceException(ErrorMessages.MOVE_INDEX_TAKEN);
        }
    }

    private static void evaluateWinner(Move move, Game game) {
        int n = 2;
        String[][] board = game.getBoard();
        int i;
        for (i = 0; i <= n; i++) {
            if (!board[move.getRow()][i].equals(move.getPlayerType().playerValue())) {
                break;
            }
        }
        if (i == n) {
            game.setWinnerStatus(move.getPlayerType().name());
            return;
        }

        for (i = 0; i <= n; i++) {
            if (!(board[i][move.getColumn()]).equals(move.getPlayerType().playerValue())) {
                break;
            }
        }
        if (i == n) {
            game.setWinnerStatus(move.getPlayerType().name());
            return;
        }

        if (move.getRow() == move.getColumn()) {
            for (i = 0; i <= n; i++) {
                if (!(board[i][i]).equals(move.getPlayerType().playerValue())) {
                    break;
                }
            }
            if (i == n) {
                game.setWinnerStatus(move.getPlayerType().name());
                return;
            }
        }


        if (move.getRow() + move.getColumn() == n) {
            for (i = 0; i <= n; i++) {
                if (!(board[i][n - i]).equals(move.getPlayerType().playerValue())) {
                    break;
                }
            }
            if (i == n) {
                game.setWinnerStatus(move.getPlayerType().name());
                return;
            }
        }

        if (game.getTotalMoves() == 8) {
            game.setWinnerStatus("GAME DRAW");
        }
    }

}
