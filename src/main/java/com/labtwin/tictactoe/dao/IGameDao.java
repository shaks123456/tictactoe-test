package com.labtwin.tictactoe.dao;

import com.labtwin.tictactoe.dto.Game;

import java.util.List;

public interface IGameDao {
    Game getGame(String gameId);

    Game upsertGame(Game game);

    List<Game> scan(int limit);
}
