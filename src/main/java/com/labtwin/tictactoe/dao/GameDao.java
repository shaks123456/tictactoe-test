package com.labtwin.tictactoe.dao;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.labtwin.tictactoe.dto.Game;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class GameDao implements IGameDao {

    private final DynamoDBMapper dynamoDBMapper;

    @Autowired
    public GameDao(DynamoDBMapper dynamoDBMapper) {
        this.dynamoDBMapper = dynamoDBMapper;
    }

    public Game getGame(String gameId) {
        return dynamoDBMapper.load(Game.class, gameId);
    }

    @Override
    public Game upsertGame(Game game) {
        dynamoDBMapper.save(game);
        return game;
    }

    @Override
    public List<Game> scan(int limit) {
        final DynamoDBScanExpression paginatedScanListExpression = new DynamoDBScanExpression()
                .withLimit(limit);
        return dynamoDBMapper.scan(Game.class, paginatedScanListExpression);
    }
}

